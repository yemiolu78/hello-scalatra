package com.qa

import org.scalatra.ScalatraServlet
import org.scalatra.scalate.ScalateSupport

// This File Edited by Ifeoluwa O....

class MainServlet extends ScalatraServlet with ScalateSupport {
  before() {
    contentType = "text/html"
  }

  get("/") {
    layoutTemplate("/WEB-INF/templates/views/index.ssp")
  }

  get("/dinosaur") {
    layoutTemplate("/WEB-INF/templates/views/dinosaur.ssp")
  }
}
